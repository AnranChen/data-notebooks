from torch import nn
import torch

EMBED_DIM = 5 #* embedded layer dimension
EMBED_ATTRI_NUM = 3 #* number of embeded attributes 
ATOM_ATTRI_NUM = 1 #* number of atom attributes (do NOT need embedding)
LSTM_INPUT_SIZE = 64 #* lstm input layer size
LSTM_HIDDEN_SIZE = 64 #* lstm hidden layer size

class LSTMModel(nn.Module):
    def __init__(self, reason_size, merchant_size, bank_size):
        '''model layer declaration
        
        '''
        super(LSTMModel, self).__init__()
    
        self.embed_reason = nn.Embedding(num_embeddings=reason_size, embedding_dim=EMBED_DIM)
        self.embed_merchant = nn.Embedding(num_embeddings=merchant_size, embedding_dim=EMBED_DIM)
        self.embed_bank = nn.Embedding(num_embeddings=bank_size, embedding_dim=EMBED_DIM)

        self.lstm = nn.LSTM(
            input_size=EMBED_DIM*EMBED_ATTRI_NUM+ATOM_ATTRI_NUM,
            hidden_size=LSTM_HIDDEN_SIZE,
            num_layers=2,
            bias=True,
            batch_first=True,
            dropout=0,
            bidirectional=False,
        )

        self.linear1 = nn.Linear(
            in_features=LSTM_HIDDEN_SIZE*2, 
            out_features=64
        )

        self.linear2 = nn.Linear(
            in_features=64, 
            out_features=1
        )

        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()

    def forward(self, input, log_lengths):
        '''feed forward algorithm

        args:
            input (tensor): input tensor for reference
            log_lengths (tensor): record of length of log for each item in batch 
                (e.g. if a batch of 4 contains logs with length of (1,2,3,4), then log_lengths = [1,2,3,4])
        
        '''
        #* deconstruct input into tensors of attributes
        x_reason, x_merchant, x_bank, x_duration = input[:,0], input[:,1], input[:,2], input[:,3]

        #* embed required attributes, and perform dimension correction for unrequired attribute
        x_reason = self.embed_reason(x_reason)
        x_merchant = self.embed_merchant(x_merchant)
        x_bank = self.embed_bank(x_bank)
        x_duration = x_duration.unsqueeze(2)

        #* concatenate all attributes to a big tensor and perform packing
        x = torch.cat((x_reason, x_merchant, x_bank, x_duration), dim=2)
        x = nn.utils.rnn.pack_padded_sequence(input=x, lengths=log_lengths, batch_first=True)

        #* continue forward propagation, extract last 2 lstm hidden layers for classification
        _, (hidden, _) = self.lstm(x)
        x = torch.cat((hidden[-2,:,:], hidden[-1,:,:]), dim=1)

        x = self.linear1(x)
        x = self.relu(x)
        x = self.linear2(x)
        x = self.sigmoid(x)

        return x

def test():
    '''testing method to confirm output tensor dimension
    
    '''
    lstm_model = LSTMModel(
        reason_size=50,
        merchant_size=60,
        bank_size=100
    )

    x = torch.randint(40, (3, 4, 9))
    log_length = torch.FloatTensor([7, 5, 3])
    print(lstm_model(x, log_length).shape == (3, 1))

    return

test()
