import matplotlib.pyplot as plt
import numpy as np
import pyspark.sql.functions as F
from pyspark.sql.functions import when, col

def format_col_name(dataframe):
    '''convert column names of a dataframe to lowercase

    '''
    col_names = dataframe.schema.names
    for col_name in col_names:
        dataframe = dataframe.withColumnRenamed(col_name, col_name.lower().replace(' ', '_'))

    return dataframe

def get_col_sum(dataframe, attribute):
    '''get the sum of a column of a dataframe

    '''
    return dataframe.agg(F.sum(attribute)).collect()[0][0]

def col_to_list(dataframe, attribute):
    '''convert a column of a dataframe to list

    '''
    return [row[0] for row in dataframe.select(attribute).collect()]

def plot_pie(dataframe, sum_col, label_col):
    '''plot pie chart of given attribute and label from a given dataframe

    '''
    plt.pie(
        col_to_list(dataframe, sum_col),
        labels=col_to_list(dataframe, label_col)
    )
    plt.show()

def plot_histogram(dataframe, attribute, binStep=100, log=False):
    '''plot histogram of certain attribute

    '''
    ls_attribute = [row[0] for row in dataframe.select(attribute).collect()]

    interval = max(ls_attribute) / binStep
    bins = np.arange(0, max(ls_attribute), interval)
    y, x, _ = plt.hist(ls_attribute, bins=bins, log=log)

    plt.xlim([0, x.max()])
    plt.ylim([0, y.max()])
    plt.show()
    return

def get_avg_risk(keys):
    '''get average risk across multiple columns

    '''
    cols = 0
    for key in keys:
        cols = cols + col(key + '_percentile')
    cols = cols/len(keys)
    return cols

def get_risk_interval(column):
    '''compute 5-step risk profile for a column

    '''
    return when(col(column) <= 20, '1_very_low')\
        .when(col(column) <= 40, '2_low')\
        .when(col(column) <= 60, '3_medium')\
        .when(col(column) <= 80, '4_high')\
        .when(col(column) <= 100, '5_very_high')

def df_to_csv(dataframe, outFilePath):
    '''save dataframe to csv

    '''
    return
