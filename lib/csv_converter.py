import pandas as pd
import os

def merge_xlsx_to_csv(category):
    '''merge xlsx from all banks into an integrated csv

    '''
    category_map = {
        'inventory': 'inventory_report',
        'items': 'field_items_report'
    }
    banks = ['absa', 'sbsa', 'dashpay', 'fnb']

    category_chosen = category_map[category]
    df = pd.DataFrame()

    for bank in banks:
        file_path = '../../spreadsheet/{}/{}_{}.xlsx'.format(bank, bank, category_chosen)

        if os.path.isfile(file_path):
            if category == 'inventory':
                df_temp = pd.read_excel(file_path, header=1, engine='openpyxl') 
            elif category == 'items':
                df_temp = pd.read_excel(file_path, sheet_name='FieldItems', header=1, engine='openpyxl') 

            df_temp['bank'] = bank
            df = df.append(df_temp)

    df.to_csv('../out/{}_all.csv'.format(category_chosen), index=False)

def read_sbsa_repairs():
    '''read sbsa repairs data 
    
    '''
    file_path = '../../spreadsheet/sbsa/SBSA Data AWS.xlsx'
    sheets = ['Swop Out', 'Support', 'Received', 'Dispatched']
    for sheet in sheets:
        df = pd.read_excel(file_path, sheet_name=sheet, header=0, engine='openpyxl')
        df.to_csv('../bin/sbsa/{}_sbsa.csv'.format(sheet.replace(' ', '').lower()), index=False)
    return

def read_sbsa_inventory():
    file_path = '../../spreadsheet/sbsa/sbsa_inventory_report.xlsx'
    df = pd.read_excel(file_path, sheet_name='SBSA Inventory Report', header=1, engine='openpyxl')
    df.to_csv('../july_sbsa/sbsa_inventory.csv', index=False)

    return

def read_sbsa_items():
    file_path = '../../spreadsheet/sbsa/sbsa_field_items_report.xlsx'
    df = pd.read_excel(file_path, sheet_name='SBSA FieldItems', header=1, engine='openpyxl')
    df.to_csv('../july_sbsa/sbsa_items.csv', index=False)

read_sbsa_items()