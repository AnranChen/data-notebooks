import  matplotlib.pyplot       as plt
import  numpy                   as np
import  pandas                  as pd   
import  pyspark  
from pyspark.sql import Window
import  pyspark.sql.functions   as F 
import pyspark.sql.types as T
 
def create_table_index(dataframe, name_index, col_reference):
    ''' create auto-incrementing index for a dataframe
    
    args:
        dataframe (spark.dataframe): dataframe to add index
        name_index (str): name of the index column
        col_reference (str): name of the column to order by
        
    returns:
        (spark.dataframe): indexed dataframe
    
    '''
    w = Window.orderBy(col_reference)
    df_old_schema = dataframe.schema.names
    df_indexed = dataframe.withColumn(name_index, F.row_number().over(w).cast(T.LongType()))\
        .select([name_index] + df_old_schema)\
    
    return df_indexed

def read_excel_sheet(inF, sheet , cols, schema, spark_session):
    df_in = pd.read_excel(inF,engine="openpyxl",sheet_name=[sheet],dtype={})  
    if (not isinstance(df_in, pd.DataFrame)):
        df_in = df_in[sheet]
    df = pd.DataFrame()
    for col in cols: 
        df[col] = df_in[col]
    #print(df_in)
    return spark_session.createDataFrame(df,schema=schema)

def read_csv(inF,cols,schema,spark_session): 
    df_spark = spark_session.read.csv(path=inF, inferSchema=True, header=True)
    original_columns = df_spark.columns
    for col in cols:
        df_spark = df_spark.withColumn(col.replace(' ',''),F.regexp_replace(F.col(col), '["=]', '').cast(schema))
    df_spark.drop(*original_columns)
    df_schema = spark_session.createDataFrame(df_spark.rdd, schema=schema)
    return df_schema

def parquet_to_excel(inF,spark_session,outF): 
    spark_session.read.parquet(inF).toPandas().to_excel(outF,engine="openpyxl")