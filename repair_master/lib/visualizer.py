import pandas
import pyspark 
import pyspark.sql
from pyspark.sql import SparkSession, DataFrame
import pyspark.sql.types as T
import pyspark.sql.functions as F
import math 
import matplotlib.pyplot as plt 
import numpy as np

def visualise_dataframe(dataframe):
    dataframe.printSchema()
    print('Number of records: {}'.format(str(dataframe.select(F.col('*')).count())))
    dataframe.show(1,truncate=False,vertical=True)


def create_repair_ration(dataframe):
    return dataframe\
        .groupBy('repairs')\
        .count()\
        .withColumnRenamed('count','total')\
        .join(
            dataframe\
                .where(F.col('scrapped') == False)\
                .groupBy('repairs')\
                .count()\
                .withColumnRenamed('count','not_scrapped'),
            ['repairs'],
            'left'
        )\
        .join(
            dataframe\
                .where(F.col('scrapped') == True)\
                .groupBy('repairs')\
                .count()\
                .withColumnRenamed('count','scrapped'),
            ['repairs'],
            'left'
        )\
        .withColumn('not_scrapped',F.when(F.col('not_scrapped').isNotNull(),F.col('not_scrapped')/F.col('total')*100).otherwise(F.lit(0)))\
        .withColumn('scrapped',F.when(F.col('scrapped').isNotNull(),F.col('scrapped')/F.col('total')*100).otherwise(F.lit(0)))\
        .sort(F.col('repairs'),ascending=True)


def visualise_repair_ratio(dataframe):
    dataframe\
        .groupBy('repairs')\
        .count()\
        .withColumnRenamed('count','total')\
        .join(
            dataframe\
                .where(F.col('scrapped') == False)\
                .groupBy('repairs')\
                .count()\
                .withColumnRenamed('count','not_scrapped'),
            ['repairs'],
            'left'
        )\
        .join(
            dataframe\
                .where(F.col('scrapped') == True)\
                .groupBy('repairs')\
                .count()\
                .withColumnRenamed('count','scrapped'),
            ['repairs'],
            'left'
        )\
        .withColumn('not_scrapped',F.when(F.col('not_scrapped').isNotNull(),F.col('not_scrapped')/F.col('total')*100).otherwise(F.lit(0)))\
        .withColumn('scrapped',F.when(F.col('scrapped').isNotNull(),F.col('scrapped')/F.col('total')*100).otherwise(F.lit(0)))\
        .sort(F.col('scrapped'),ascending=False)\
        .show(100,truncate=False)



def plot_histogram(dataframe, attribute, binStep=10, log=True):
    '''plot histogram of certain attribute

    '''
    ls_attribute = [row[0] for row in dataframe.select(attribute).collect()]

    interval = max(ls_attribute) / binStep
    bins = np.arange(0, max(ls_attribute), interval)
    y, x, _ = plt.hist(ls_attribute, bins=bins, log=log)

    plt.xlim([0, x.max()])
    plt.ylim([0, y.max()])
    plt.show()