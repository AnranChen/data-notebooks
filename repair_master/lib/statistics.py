import pandas                   as pd
import math
import matplotlib
import matplotlib.pyplot        as plt
import numpy                    as np
import warnings 
import statsmodels.api          as sm
from statsmodels.tsa.stattools  import adfuller
from statsmodels.tsa.api        import SimpleExpSmoothing, Holt, ExponentialSmoothing
# Ignore the warning, can comment this 
warnings.filterwarnings("ignore")
pd.options.display.float_format = '{:.8f}'.format
# For graphing purpose, can change 
plt.style.use('seaborn-bright')
matplotlib.rcParams['axes.labelsize'] = 14
matplotlib.rcParams['xtick.labelsize'] = 12
matplotlib.rcParams['ytick.labelsize'] = 12
matplotlib.rcParams['text.color'] = 'k'

def plot_rolling_means(
    y,
    resample_label_tuple=
    [
        (1,'Monthly Mean Resample'),
        (3,'Qaurterly Mean Resample'),
        (4,'Triannually Mean Resample'),
        (6,'Biannually Mean Resample'),
        (12,'Yearly Mean Resample'),
    ],
    fig_size=(20,6)
    ):
    fig,ax  = plt.subplots(figsize=fig_size)
    for resample,plot_label in resample_label_tuple:
        ax.plot(
            y.resample('{}M'.format(resample)).mean(),
            marker='o',
            markersize=resample,
            linestyle='-',
            label=plot_label
        )
    ax.set_ylabel('Repairs')
    ax.legend()

def seasonal_decompose(
    y,
    period=1,
    sm_model='additive',
    fig_size=(14,7)
    ):
    decomposition = sm.tsa.seasonal_decompose(
        y,
        model=sm_model,
        period=60,
        extrapolate_trend='freq'
        )
    fig = decomposition.plot()
    w,h = fig_size
    fig.set_size_inches(w,h)
    plt.show() 

def test_stationarity(y,title,rolling_window=1,fig_size=(16,9)):
    #Determing rolling statistics
    rolmean = pd.Series(y).rolling(window=rolling_window).mean() 
    rolstd  = pd.Series(y).rolling(window=rolling_window).std()
    
    fig, ax = plt.subplots(figsize=fig_size)
    ax.plot(y, label= title)
    ax.plot(rolmean, label='rolling mean');
    ax.plot(rolstd, label='rolling std (x10)');
    ax.legend()

def ADF_test(y, title):
    print(' > Is the {} stationary ?'.format(title))
    dftest = adfuller(y.dropna(), autolag='AIC')
    print('Test statistic = {:.3f}'.format(dftest[0]))
    print('P-value = {:.3f}'.format(dftest[1]))
    print('Critical values :')
    for k, v in dftest[4].items():
        print('\t{}: {} - The data is {} stationary with {}% confidence'.format(k, v, 'not' if v<dftest[0] else '', 100-int(k[:-1])))

def plot_rolling_means(
    y,
    resample_label_tuple=
    [
        (1,'Monthly Mean Resample'),
        (3,'Qaurterly Mean Resample'),
        (4,'Triannually Mean Resample'),
        (6,'Biannually Mean Resample'),
        (12,'Yearly Mean Resample'),
    ],
    fig_size=(16,9)
    ):
    fig,ax  = plt.subplots(figsize=fig_size)
    for resample,plot_label in resample_label_tuple:
        ax.plot(
            y.resample('{}M'.format(resample)).mean(),
            marker='o',
            markersize=resample,
            linestyle='-',
            label=plot_label
        )
    ax.set_ylabel('Repairs')
    ax.legend()


def simple_exponential_smoothing(
    y,
    y_to_train,
    y_to_test,
    smoothing_level,
    predict_date,
    fig_size = (16,9)
):
    fig,ax  = plt.subplots(figsize=fig_size)
    ax.plot(y,marker='o',color='black',markersize=1,linestyle='-',label='original data')

    # specific smoothing level
    fit1    =   SimpleExpSmoothing(
                    y_to_train
                )\
                .fit(
                    smoothing_level=smoothing_level,
                    optimized=False
                )
    fcast1  =   fit1.forecast(
                    predict_date
                ) 
    ax.plot(fcast1,marker='o', color='blue',label=r'$\alpha={}$'.format(smoothing_level)) 
    ax.plot(fit1.fittedvalues,marker='o',  color='blue') 
    mse1 = ((fcast1 - y_to_test) ** 2).mean()
    print('The Root Mean Squared Error of our forecasts with smoothing level of {} is {}'.format(smoothing_level,round(np.sqrt(mse1), 2)))
    ## auto optimization
    fit2    =   SimpleExpSmoothing(
                    y_to_train
                )\
                .fit()
    fcast2  =   fit2.forecast(
                    predict_date
                )
    ax.plot(fcast2,marker='o', color='green',label=r'$\alpha=%s$'%fit2.model.params['smoothing_level']) 
    ax.plot(fit2.fittedvalues,marker='o',  color='green') 
    mse2 = ((fcast2 - y_to_test) ** 2).mean()
    print('The Root Mean Squared Error of our forecasts with auto optimization is {}'.format(round(np.sqrt(mse2), 2)))
    ax.set_ylabel('Repairs')
    ax.legend() 

def holt(
    y,
    y_to_train,
    y_to_test,
    smoothing_level,
    smoothing_slope, 
    predict_date,
    fig_size = (16,9)
    ):
    fig,ax  = plt.subplots(figsize=fig_size)
    ax.plot(y,marker='o', color='black',markersize=1,linestyle='-',label='original data' )
     
    fit1    = Holt(y_to_train).fit(smoothing_level, smoothing_slope, optimized=False)
    fcast1  = fit1.forecast(predict_date).rename("Holt's linear trend")
    mse1    = ((fcast1 - y_to_test) ** 2).mean()
    print('The Root Mean Squared Error of Holt\'s Linear trend {}'.format(round(np.sqrt(mse1), 2)))
    ax.plot(fit1.fittedvalues,marker='o',color='blue',markersize=1,linestyle='-',label='Holt\'s linear trend' )
    ax.plot(fcast1,marker='o',color='blue')
    """
    to used exponential Holt's, no zero values
    """
    fit2    = Holt(y_to_train, exponential=True).fit(smoothing_level, smoothing_slope, optimized=False)
    fcast2  = fit2.forecast(predict_date).rename("Exponential trend")
    mse2    = ((fcast2 - y_to_test) ** 2).mean()
    print('The Root Mean Squared Error of Holt\'s Exponential trend {}'.format(round(np.sqrt(mse2), 2)))
    ax.plot(fit2.fittedvalues,marker='o',color='red',markersize=1,linestyle='-',label='Exponential trend')
    ax.plot(fcast2,marker='o',color='red')
    ax.set_ylabel('Repairs')
    ax.legend()  