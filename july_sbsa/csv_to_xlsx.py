import os
import pandas as pd

csv_files = os.listdir('csv')
for csv_file in csv_files:
    df = pd.read_csv('./csv/{}'.format(csv_file))
    df.to_excel('./xlsx/{}.xlsx'.format(csv_file.split('.')[0]), index=False)
